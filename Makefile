MAUDE = maude
MAUDE_OPTS = -no-banner -no-wrap -batch

# tests.maude must come first!
SRCS = node.maude node-preds.maude
TEST_SRCS = $(wildcard test-*.maude)

ALL_TESTS = $(TEST_SRCS:.maude=.test)

test: $(ALL_TESTS)

test-%.test: test-%.actual.out test-%.expected.out
	diff $^

test-%.actual.out: test-%.maude %.maude tmp-setup.maude
	$(MAUDE) $(MAUDE_OPTS) tmp-setup.maude $< < /dev/null > $@

test-%.expected.out: test-%.maude %.maude tmp-setup.maude
	echo "WARNING: Writing test reference output $@"
	$(MAUDE) $(MAUDE_OPTS) tmp-setup.maude $< < /dev/null > $@
	chmod -w $@

.PHONY: clean clean-actual-out clean-expected-out test #$(ALL_TESTS)
.PRECIOUS: $(subst .test,.expected.out,$(ALL_TESTS))
.SUFFIXES:

tmp-setup.maude:
	echo > $@
	echo "set show stats off ." >> $@
	echo "set show timing off ." >> $@

clean: clean-actual-out
	rm -f tmp-setup.maude

clean-actual-out:
	rm -f $(subst .maude,.actual.out,$(TEST_SRCS))

clean-expected-out:
	rm $(subst .maude,.expected.out,$(TEST_SRCS))
