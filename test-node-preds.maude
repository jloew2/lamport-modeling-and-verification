load node-preds.maude

view 4 from NZNAT# to NAT is
  op # to term 4 .
endv

--- Kshemkalyani and Singhal (p. 6) lists the requirements of a mutual exclusion algorithm:
--- * Safety Property: At any instant, only one process can execute the critical section.
--- * Liveness Property: This property states the absence of deadlock and starvation. Two or more sites should not endlessly wait for messages which will never arrive.
--- * Fairness: Each process gets a fair chance to execute the CS. Fairness property generally means the CS execution requests are executed in the order of their arrival (time is determined by a logical clock) in the system.

--- Check the mutual exclusion property
red modelCheck(init, []~ twoInCritSec) .
--- Expected: true

--- Check that each node can send all its messages. This is our equivalent of
--- the strong liveness property.
red modelCheck(init, <>[]~ hasUnsentMessages) .
--- Expected: true

--- Check for fairness.
red modelCheck(init, [] ~ isUnfair) .
--- Expected: true
