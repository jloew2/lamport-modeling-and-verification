--- TODO: Extract MatchForm and ReachForm into their own file; clarify the sorting
---       requirements on their handler functions
--- TODO: MATCH-FORM-AUX is actually implementing a general formula simplification
---       but that fact is really unclear because of the terrible module name;
---       this code needs to move into foform.maude and either get proper support
---       for disjunction or explicitly declare its input as Conj
--- TODO: finish redoing the MatchForm/ReachForm to use EqQFForm? as the base
---       formula structure
--- TODO: rework unification and goal generation to preserve variable names
---       whenever possible --- IDEA: check if in the unifier an original
---       variable is mapped to another variable (perhaps of a lower sort)
---       if so, preserve the name --- if not, then change the name to fresh
---       names

fmod SYN-SUBSUMPTION-CHECKER is
  pr REACH-FORM .
  pr UNIQUE-PREFIX .

  --- TODO: Lift this functionality into full MatchAtom/MatchForm pairs by disjunction
  --- NOTE: Is the above lifting necessary? Just get it to work for the simple case first?
  --- subsume one match atom against another
  op syn-sub     : Module MatchForm? MatchForm? -> Bool .
  op syn-sub     : Module MatchAtom  MatchAtom  -> Bool .
  --- extend a signature with new operators for formulas
  op formula-ext : Module        -> Module .
  op form-ext    : Qid           -> OpDeclSet .
  op eq-ext      : Qid Qid Sort  -> OpDeclSet .
  --- operator name generation
  op eq-op       : Qid           -> Qid .
  op neq-op      : Qid           -> Qid .
  op tt-op       : Qid           -> TermQid .
  --- convert a formula into a term in the extended signature
  op to-term     : Module  MatchAtom -> Term .
  op to-term     : Module  QFForm?   -> Term .
  op to-term     : Qid Qid MatchAtom -> Term .
  op to-term     : Qid Qid QFForm?   -> Term .

  var F F' : ReachForm . var MA MA' : MatchAtom . var MF MF' : MatchForm . var MF? MF'? : MatchForm? .
  var B : Bool . var U : Module . var QF QF' : QFForm . var T T' : Term .
  var S : Sort . var SS : SortSet . var QF? : QFForm? . var Q Q' : Qid .
  ---
  eq syn-sub(U,T | QF?,MA)   = metaMatch(formula-ext(U),to-term(U,T | QF?),to-term(U,MA),nil,0) :: Substitution or-else
                              metaMatch(formula-ext(U),'|[T,'/\[to-term(U,QF?),join('F: qid(sortPrefix(U)) 'Form)]],to-term(U,MA),nil,0) :: Substitution .
  eq syn-sub(U,MA U MF?,MA') = syn-sub(U,MA,MA') or-else syn-sub(U,MF?,MA') .
  eq syn-sub(U,MF?,MF'?)     = false [owise] .
  ---
  eq formula-ext(U)          =  addSorts(join(qid(sortPrefix(U)) 'Form),addOps(eq-ext(qid(opPrefix(U)),qid(sortPrefix(U)),getSorts(U)) form-ext(qid(sortPrefix(U))),U)) .
  eq form-ext(Q)             = (op '/\       : join(Q 'Form) join(Q 'Form) -> join(Q 'Form) [assoc comm id(join('tt. Q 'Form))].
                                op '\/       : join(Q 'Form) join(Q 'Form) -> join(Q 'Form) [assoc comm].
                                op 'tt       : nil                         -> join(Q 'Form) [none].) .

  eq eq-ext(Q,Q',S ; SS)     = (op '|        : S join(Q 'Form) -> join(Q' 'Form) [none].
                                op eq-op (Q) : S S             -> join(Q' 'Form) [none].
                                op neq-op(Q) : S S             -> join(Q' 'Form) [none].) eq-ext(Q,Q',SS) .
  eq eq-ext(Q,Q',none)       = none .
  ---
  eq eq-op  (Q)              = join(Q '?=) .
  eq neq-op (Q)              = join(Q '!=) .
  eq tt-op  (Q)              = join('tt. Q 'Form) .
  ---
  eq to-term(U,QF?)          = to-term(qid(opPrefix(U)),qid(sortPrefix(U)),QF?) .
  eq to-term(U,MA)           = to-term(qid(opPrefix(U)),qid(sortPrefix(U)),MA) .
  eq to-term(Q,Q',T | QF)    = '|[T,to-term(Q,Q',QF)] .
  eq to-term(Q,Q',T | tt)    = '|[T,tt-op(Q')] .
  eq to-term(Q,Q',QF /\ QF') = '/\[to-term(Q,Q',QF),to-term(Q,Q',QF')] .
  eq to-term(Q,Q',QF \/ QF') = '\/[to-term(Q,Q',QF),to-term(Q,Q',QF')] .
  eq to-term(Q,Q',T ?= T')   = eq-op (Q)[T,T'] .
  eq to-term(Q,Q',T != T')   = neq-op(Q)[T,T'] .
  eq to-term(Q,Q',mtForm)    = tt-op(Q') .
endfm

fmod DEFINED-SUBTERMS is
  pr META-LEVEL .
  pr MGCI .         --- used to find ctor subterms
  pr GEN-VARNAMES . --- used to generate fresh variables
  pr QID-JOIN .

  --- used for term abstraction
  sort AbstractionData .
  op ((_,_,_)) : Nat Term Substitution -> AbstractionData [ctor] .

  op  defined-abs : Module Term -> AbstractionData .
  op $dabs        : Module AbstractionData -> AbstractionData .
  op $dabsL       : Module Qid AbstractionData TermList TermList -> AbstractionData .
  op $bind        : Nat Module Term Substitution -> AbstractionData .
  op $bind        : Nat Variable Term Substitution -> AbstractionData .
  op tl2Vars      : Module TermList -> TermList .


  var TL TL' : TermList .
  var T T'   : Term .
  var M      : Module .
  var C      : Constant .
  var V      : Variable .
  var Q      : Qid .
  var N      : Nat .
  var S      : Substitution .

  --- INP: Module Term
  --- PRE: Term is well-defined in Module
  --- OUT: AbstractionData (Nat,Term,Substitution) where
  ---      all minimal defined subterms have been extracted
  ---      and replaced by tmp variables; the substitution
  ---      maps each new tmp variable to its original removed subterm
  eq  defined-abs(M,T)      = $dabs(M,(0,T,none)) .
  eq $dabs(M,(N,V,S))       =  (N,V,S) .
  eq $dabs(M,(N,C,S))       =  if N == 0 and-then not ctor-term?(M,C) then $bind(N,M,C,S) else (N,C,S) fi .
  eq $dabs(M,(N,Q[(T,TL)],S)) =
        if not ctor-term?(M,Q[tl2Vars(M,(T,TL))]) then
          $bind(N,M,Q[(T,TL)],S)
        else
          $dabsL(M,Q,$dabs(M,(N,T,S)),TL,empty)
        fi .
  eq $dabsL(M,Q,(N,T,S),(T',TL),TL') = $dabsL(M,Q,$dabs(M,(N,T',S)),TL,(TL',T)) .
  eq $dabsL(M,Q,(N,T,S),empty,TL')   = (N,Q[TL',T],S) .

  eq $bind(N,M,T,S) = $bind(N,tmpvar(N,M,T),T,S) .
  eq $bind(N,V,T,S) = (s(N),V,V <- T ; S) .

  eq  tl2Vars(M,(T,TL)) = join('X: leastSort(M,T)),tl2Vars(M,TL) .
  eq  tl2Vars(M,empty)  = empty .
endfm

fmod ABS-REW-RULES is
  pr REACH-FORM .
  pr UNIFIERS .
  pr DEFINED-SUBTERMS .

  --- Sorts for abstracted rewrite rules/abstracted rule & substitution pairs
  sort AbsRewRule AbsRewRuleSet .
  subsort AbsRewRule < AbsRewRuleSet .
  sort RuleSubst RuleSubstSet .
  subsort RuleSubst < RuleSubstSet .
  op rl_:_=>_if_and_. : Qid Term Term EqConj? EqConj? -> AbsRewRule [ctor format (d d d d d d s d d s d n)] .
  op __               : AbsRewRuleSet AbsRewRuleSet -> AbsRewRuleSet [ctor assoc comm id: none] .
  op none             : -> AbsRewRuleSet .
  op ((_,_))          : AbsRewRule Substitution   -> RuleSubst    [ctor] .
  op _|_              : RuleSubstSet RuleSubstSet -> RuleSubstSet [ctor assoc comm id: none format(d d n d)] .
  op none             : -> RuleSubstSet .

  --- create abstract rule set
  op  abstract-rules  : Module -> AbsRewRuleSet .
  op  abstract-rules  : Module RuleSet -> AbsRewRuleSet .
  op $abs             : Module Rule -> AbsRewRule .
  op $abs             : AbstractionData Rule -> AbsRewRule .
  op  get-label       : AttrSet -> Qid .
  --- abstract rule projections
  op  rhs             : AbsRewRule -> Term .
  op  cond            : AbsRewRule -> Conj .
  --- auxiliary operators
  op  applySub        : AbsRewRule Substitution -> AbsRewRule .
  op  subst2Conj      : Substitution -> PosEqConj? .
  op  eqCond2Conj     : EqCondition ~> EqConj? .

  var Q : Qid . var V : Variable . var RLS : RuleSet . var EC : EqCondition .
  var N : Nat . var M : Module   . var AS  : AttrSet . var RF : ReachForm   .
  var L R R' T T' : Term . var ARS ARS' : AbsRewRuleSet . var RL : Rule .
  var S S' : Substitution . var AR : AbsRewRule . var C P : EqConj? .

  --- Abstracts rewrite rules
  eq  abstract-rules(M)                    = abstract-rules(M,getRls(M)) .
  eq  abstract-rules(M,RL RLS)             = abstract-rules(M,RLS) $abs(M,RL) .
  eq  abstract-rules(M,none)               = none .
  eq $abs(M,rl L => R [AS].)               = $abs(defined-abs(M,R),rl L => R [AS].) .
  eq $abs(M,crl L => R if EC [AS].)        = $abs(defined-abs(M,R),crl L => R if EC [AS].) .
  eq $abs((N,R',S),rl L => R [AS].)        = (rl get-label(AS) : L => R' if mtForm and subst2Conj(S) .) .
 ceq $abs((N,R',S),crl L => R if EC [AS].) = (rl get-label(AS) : L => R' if C      and subst2Conj(S) .)
  if  C := eqCond2Conj(EC) .

  --- Projections
  eq  rhs(rl Q : L => R if C and P .)  = R .
  eq  cond(rl Q : L => R if C and P .) = C /\ P .

  --- Auxiliary operators
  eq  eqCond2Conj('_=/=_[T,T'] = 'tt.Bool* /\ EC) = T != T' /\ eqCond2Conj(EC) .
  eq  eqCond2Conj('_==_[T,T']  = 'tt.Bool* /\ EC) = T ?= T' /\ eqCond2Conj(EC) .
  eq  eqCond2Conj(T = T' /\ EC)                   = T ?= T' /\ eqCond2Conj(EC) [owise] .
  eq  eqCond2Conj(nil)                            = mtForm .
  eq  subst2Conj(V <- T ; S)                      = V ?= T /\ subst2Conj(S) .
  eq  subst2Conj(none)                            = mtForm .
  eq  applySub(rl Q : L => R if C and P .,S)      = rl Q : L << S => R << S if C << S and P << S . .
  eq  get-label(label(Q) AS)                      = Q .
  eq  get-label(AS)                               = 'none [owise] .
endfm

--- This module is designed to check three kinds of error conditions
--- [1] Are all the rules in the module we are analyzing in the same kind?
---     If not, that means we cannot be topmost, since there are rules over different kinds
--- [2] Are all of the terms at the head of a MatchAtom the same kind as the
---     kind of all the semantic rules in the module? If not, it means our
---     formula is erroneous
--- [3] Are all of the dis/equalities between two terms over two terms in the same kind?
---     If not, then the equality is illformed
fmod RLTL-RULE-CHECK is
  pr META-LEVEL .
  pr REACH-FORM .

  op rule-check  : Module -> Bool .
  op rule-check  : Module RuleSet -> Bool .
  op rule-check  : Module Type RuleSet -> Bool .
  op form-check  : Module ReachFormExSet -> Bool .
  op fc          : Module Type ReachFormExSet -> Bool .
  op fc          : Module Type MatchForm -> Bool .
  op fc          : Module Type FOForm? -> Bool .
  op fc          : Module Type Term -> Bool .
  op getRuleType : Module ~> Type .
  op getRuleType : Module RuleSet ~> Type .

  var M : Module . var L R T1 T2 : Term . var F? : FOForm? . var F1 F2 : FOForm . var T : Type .
  var RLS : RuleSet . var MF MF' : MatchForm . var MA : MatchAtom . var AS : AttrSet .
  var RF RF' : ReachFormEx . var RFS : ReachFormExSet .

  eq rule-check(M) = rule-check(M,getRls(M)) .
  eq rule-check(M,none) = false .
  eq rule-check(M,rl L => R [AS] . RLS) =
    leastSort(M,L) ; leastSort(M,R) :: NeSortSet and-then sameKind(M,leastSort(M,L),leastSort(M,R)) and-then rule-check(M,leastSort(M,L),RLS) .
  eq rule-check(M,crl L => R if C:Condition [AS] . RLS) =
    leastSort(M,L) ; leastSort(M,R) :: NeSortSet and-then sameKind(M,leastSort(M,L),leastSort(M,R)) and-then rule-check(M,leastSort(M,L),RLS) .
  eq rule-check(M,T,none) = true .
  eq rule-check(M,T,rl L => R [AS] . RLS) =
    leastSort(M,L) ; leastSort(M,R) :: NeSortSet and-then sameKind(M,T,leastSort(M,L)) and-then sameKind(M,T,leastSort(M,R)) and-then rule-check(M,T,RLS) .
  eq rule-check(M,T,crl L => R if C:Condition [AS] . RLS) =
    leastSort(M,L) ; leastSort(M,R) :: NeSortSet and-then sameKind(M,T,leastSort(M,L)) and-then sameKind(M,T,leastSort(M,R)) and-then rule-check(M,T,RLS) .

  eq form-check(M,RFS)      = fc(M,getRuleType(M),RFS) .
  eq fc(M,T,RF & RF' & RFS) = fc(M,T,RF) and-then fc(M,T,RF' & RFS) .
  eq fc(M,T,MF =>A MF')     = fc(M,T,MF) and-then fc(M,T,MF') .
  eq fc(M,T,MA U MF)        = fc(M,T,MA) and-then fc(M,T,MF) .
  eq fc(M,T,T1 | F?)        = sameKind(M,T,leastSort(M,T1)) and-then fc(M,T,F?) .
  eq fc(M,T,F1 /\ F2)       = fc(M,T,F1) and-then fc(M,T,F2) .
  eq fc(M,T,F1 \/ F2)       = fc(M,T,F1) and-then fc(M,T,F2) .
  eq fc(M,T,~ F1)           = fc(M,T,F1) .
  eq fc(M,T,T1 ?= T2)       = sameKind(M,leastSort(M,T1),leastSort(M,T2)) .
  eq fc(M,T,T1 != T2)       = sameKind(M,leastSort(M,T1),leastSort(M,T2)) .
  eq fc(M,T,tt)             = true .
  eq fc(M,T,ff)             = true .
  eq fc(M,T,mtForm)         = true .

  eq getRuleType(M) = getRuleType(M,getRls(M)) .
  eq getRuleType(M,rl L => R [AS] . RLS) = leastSort(M,L) .
  eq getRuleType(M,crl L => R if C:Condition [AS] . RLS) = leastSort(M,L) .
endfm

fmod REACH-PROOF-STATE is
  pr ABS-REW-RULES .
  pr REACH-FORM .
  pr VAR-SAT .
  pr RLTL-RULE-CHECK .
  pr PATTERN-OPS .

  sort RuleTrace ReachGoal ReachGoalSet ReachProof RuleType ProofObligation ProofObligationSet .
  subsort ReachGoal < ReachGoalSet .
  subsort Nat < RuleTrace .
  --- RuleTrace
  op _@_          : RuleTrace Nat -> RuleTrace [ctor] .
  op _#_          : RuleTrace Nat -> RuleTrace [ctor] .
  op _%_          : RuleTrace Nat -> RuleTrace [ctor] .
  --- RuleType
  op  step        : -> RuleType [ctor] .
  op  axioms      : ReachFormSet -> RuleType [ctor] .
  op  axioms(...) : -> RuleType [ctor] .
  --- Errors
  op  rltl-error  : QidList RuleTrace ReachFormSet ~> ReachGoal  [ctor] .
  op  rltl-error  : QidList ReachFormSet ~> ReachGoal  [ctor] .
  op  rltl-error  : QidList MatchForm    ~> ReachGoal  [ctor] .
  op  rltl-error  : QidList ~> ReachGoal [ctor] .
  --- GoalSets
  op _&&_         : ReachGoalSet ReachGoalSet -> ReachGoalSet [format (d d nn d) ctor assoc comm id: mt] .
  op mt           : -> ReachGoalSet [ctor] .
  op [_,_,_]      : RuleTrace RuleType ReachForm -> ReachGoal [ctor format (d d d d nn d d d)] .
  --- ReachTool Proof State
  ---  [1] the original module
  ---  [2] submodule containing all symbols with finite variant property (FVP)
  ---  [3] module which contains transformation rules which implement the descent map (should protect original module)
  ---  [4] a nat marking next globally free variable
  ---  [5] a matching formula representing our terminating states
  ---  [6] a set of abstracted rewrite rules from [1] whose righthand sides do not contain defined symbols
  ---  [7] a set of reachformulas which are used as axioms
  ---  [8] a set of goals which we will try to prove during this proof step
  ---  [9] a set of goals (and possibly generated errors) generated during this proof step;
  ---      when [7] becomes empty, we replace [7] with [8]
  op [_,_,_,_,_,_,_||_||_] : Module Module Module Nat MatchForm AbsRewRuleSet ReachFormSet [ReachGoalSet] [ReachGoalSet] -> ReachProof
    [ctor format (d d d d d d d d d d d d d d nn d nn d d d)] .

  --- Initialize Proof State
  op init         : Module ReachFormSet MatchForm -> ReachProof .
  op init         : Module ReachFormSet ReachFormSet MatchForm -> ReachProof .
  op init         : Module ReachFormSet ReachFormSet ReachFormSet MatchForm -> ReachProof .
  op init         : Module Module Module ReachFormSet ReachFormSet ReachFormSet MatchForm -> ReachProof .
  op initGoals    : Nat ReachFormSet ReachFormSet -> ReachGoalSet .
  --- Run Operator
  op run          : ReachProof Bound -> ReachProof .
  --- Boolean Tests
  op error?       : ReachProof -> Bool .
  --- Project Components
  op term-set     : ReachProof -> MatchForm .
  op goals!       : ReachProof ~> ReachGoalSet .
  op goals        : ReachProof ~> ReachGoalSet .
  op goal         : ReachProof NeNatList -> ReachGoalSet .
  op goal         : ReachGoalSet NeNatList -> ReachGoalSet .
  op match        : RuleTrace NeNatList -> Bool .
  op goal-pvars   : ReachGoal -> QidSet .
  --- Inject Components
  op set-goals    : ReachProof ReachGoalSet -> ReachProof .
  op add-goals    : ReachProof ReachGoalSet -> ReachProof .
  op del-goal     : ReachProof NeNatList -> ReachProof .
  op del-goal     : ReachGoalSet ReachGoalSet -> ReachGoalSet .
  --- Create Goals
  op case-goals   : ReachGoal Nat SubstitutionSet -> ReachGoalSet .
  op split-goals  : Module ReachGoal EqQFForm? -> ReachGoalSet .
  --- Approx Diff extended to ReachForms
  op over-diff   : Module ReachForm -> QFForm? .

  var FM DM M M' M'' : Module . var AXS AXS' CRS CRS' GLS GLS' : ReachFormSet . var D D' : MatchForm . var MA : MatchAtom . var FS : QFForm?Set .
  var T T' T1 T2 T3 T4 T5 T6 : Term . var N1 N2 N3 N4 N5 N6 : Nat . var ARS ARS' : AbsRewRuleSet . var WGLS WGLS' : ReachGoalSet .
  var ERR ERR' : [ReachGoalSet] . var RP : ReachProof . var NL : NeNatList . var RF : ReachForm . var TR : RuleTrace . var V : Variable .
  var RT : RuleType . var G : ReachGoal . var S : Substitution . var SS : SubstitutionSet . var C C' : QFForm? . var QS : QidSet .

  --- INP: Module GLS:ReachFormSet AXS:(ReachFormSet) D:MatchForm
  --- PRE: Arguments are well-defined with respect to Module
  --- OUT: Initialize our proof state with goals from GLS, axioms AXS,
  ---      and terminating states D; rename everything so that name
  ---      collisions won't happen between different pieces
  eq init(M,GLS,D)             = init(M,noModule,noModule,mtrfs,GLS,GLS,D) .
  eq init(M,CRS,GLS,D)         = init(M,noModule,noModule,mtrfs,CRS,GLS,D) .
  eq init(M,AXS,CRS,GLS,D)     = init(M,noModule,noModule,AXS,CRS,GLS,D) .
 ceq init(M,FM,DM,AXS,CRS,GLS,D) = [M',FM,DM,N5,D',ARS',AXS' & CRS' || mt || WGLS]
  if rule-check(M)
  /\ form-check(M,AXS & CRS & GLS)
  /\ illFormedSet(M,AXS & CRS & GLS) == mtrfs
  /\ termdata(T1,N1) := #renameAllVar(M,notFound,upTerm(getRls(M)))
  /\ ARS             := abstract-rules(M,downTerm(T1,(none).RuleSet))
  /\ termdata(T2,N2) := #renameTmpVar(M,N1,upTerm(ARS))
  /\ termdata(T3,N3) := #renameAllVar(M,N2,upTerm(D))
  /\ termdata(T4,N4) := #renameAllVar(M,N3,upTerm(AXS))
  /\ termdata(T5,N5) := #renameAllVar(M,N4,upTerm(GLS))
  /\ termdata(T6,N6) := #renameAllVar(M,N5,upTerm(CRS))
  /\ M'              := setRls(M,downTerm(T1,(none).RuleSet))
  /\ ARS'            := downTerm(T2,(none).AbsRewRuleSet)
  /\ D'              := downTerm(T3,(''Err.Qid | mtForm))
  /\ AXS'            := downTerm(T4,mtrfs)
  /\ GLS'            := downTerm(T5,mtrfs)
  /\ CRS'            := downTerm(T6,mtrfs)
  /\ WGLS            := initGoals(1,AXS',GLS') .

 ceq init(M,FM,DM,AXS,CRS,GLS,D) = [M,FM,DM,0,D,none,mtrfs || mt || rltl-error('No 'Semantic 'Rules 'In 'Module 'Or 'Rules 'Are 'Ill-formed)] if not rule-check(M) .
 ceq init(M,FM,DM,AXS,CRS,GLS,D) = [M,FM,DM,0,D,none,mtrfs || mt || rltl-error('Formula/Rule 'Kind 'Disagreement)] if not form-check(M,AXS & CRS & GLS) .
  eq init(M,FM,DM,AXS,CRS,GLS,D) = [M,FM,DM,0,D,none,mtrfs || mt || rltl-error('Formulas 'Ill-formed,illFormedSet(M,AXS & CRS & GLS))] [owise] .

  --- I NP: ReachProof Bound
  --- P RE: None
  --- OUT: The ReachProof after taking Bound steps
  eq run([M,FM,DM,N1,D,ARS,AXS || mt   || mt],B:Bound)     =     [M,FM,DM,N1,D,ARS,AXS || mt   || mt   ] .
  eq run([M,FM,DM,N1,D,ARS,AXS || mt   || WGLS],0)         =     [M,FM,DM,N1,D,ARS,AXS || mt   || WGLS ] .
  eq run([M,FM,DM,N1,D,ARS,AXS || mt   || WGLS],s(N2))     = run([M,FM,DM,N1,D,ARS,AXS || WGLS || mt   ],N2) [owise] .
  eq run([M,FM,DM,N1,D,ARS,AXS || mt   || WGLS],unbounded) = run([M,FM,DM,N1,D,ARS,AXS || WGLS || mt   ],unbounded) [owise] .
 ceq run([M,FM,DM,N1,D,ARS,AXS || WGLS || ERR],B:Bound)    =     [M,FM,DM,N1,D,ARS,AXS || mt   || ERR  ] if not ERR :: ReachGoalSet [owise] .

  --- INP: Nat ReachFormSet
  --- PRE: None
  --- OUT: An initial ReachGoalSet where each ReachGoal
  ---      corresponds to a ReachForm in ReachFormSet
  eq initGoals(N1,AXS,(MA =>A D) & GLS) =
       if AXS == mtrfs then [N1,step,MA =>A D] else [N1,axioms(AXS),MA =>A D] fi && initGoals(s(N1),AXS,GLS) .
  eq initGoals(N1,AXS,mtrfs)            = mt .

  --- INP: ReachProof
  --- PRE: None
  --- OUT: True iff no errors were generated during the proof
  eq error?([M,FM,DM,N1,D,ARS,AXS || mt || ERR]) = not ERR :: ReachGoalSet .

  --- Inject Components
  op add-goals : ReachProof ReachGoalSet -> ReachProof .
  op del-goal  : ReachProof NeNatList -> ReachProof .

  eq term-set([M,FM,DM,N1,D,ARS,AXS || ERR || ERR']) = D .

  --- INP: ReachProof
  --- PRE: ReachGoalSet has no error terms
  --- OUT: ReachGoalSet
  eq goals([M,FM,DM,N1,D,ARS,AXS || mt || WGLS]) = WGLS .
  eq goals!([M,FM,DM,N1,D,ARS,AXS || WGLS || ERR]) = ERR .

  --- INP: ReachProof NeNatList
  --- PRE: ReachGoalSet has no error terms
  --- OUT: ReachForm corresponding to given goal or mtrfs if it does not exist
  eq goal(RP,NL) = goal(goals(RP),NL) .
  eq goal([TR,RT,RF] && WGLS,NL) = if match(TR,NL) then [TR,RT,RF] else goal(WGLS,NL) fi .
  eq goal(mt,NL)                 = mt .
  eq match(TR # N1,NL N2)        = N1 == N2 and-then match(TR,NL) .
  eq match(TR @ N1,NL N2)        = N1 == N2 and-then match(TR,NL) .
  eq match(TR % N1,NL N2)        = N1 == N2 and-then match(TR,NL) .
  eq match(N1,N2)                = N1 == N2 .
  eq match(N1,NL)                = false [owise] .

  --- INP: ReachProof ReachGoalSet
  --- PRE: ReachGoalSet has no error terms
  --- OUT: Set goals for proof as ReachGoalSet
  eq set-goals([M,FM,DM,N1,D,ARS,AXS || mt || WGLS],WGLS') =
       [M,FM,DM,N1,D,ARS,AXS || mt || WGLS'] .
  eq add-goals(RP,WGLS')   = set-goals(RP,goals(RP) && WGLS') .
  eq del-goal(RP,NL)       = set-goals(RP,del-goal(goals(RP),goal(RP,NL))) .
  eq del-goal(WGLS && G,G) = WGLS .
  eq del-goal(WGLS,mt)     = WGLS .
  eq del-goal(WGLS,G)      = WGLS [owise] .

  --- INP: ReachGoal Nat SubstitutionSet
  --- PRE: None
  --- OUT: A ReachGoalSet generated by applying the substitutions
  eq case-goals([TR,RT,RF],N1,S | SS) = [TR % N1,RT,applySub(RF,S)] && case-goals([TR,RT,RF],s(N1),SS) .
  eq case-goals([TR,RT,RF],N1,empty)  = mt .

  --- INP: Module ReachGoal EqQFForm?
  --- PRE: Formulas are well-defined with respect to module
  --- OUT: A ReachGoalSet generated by conjoining the formula and
  ---      its negation with the goal in question
  eq split-goals(M,[TR,RT,RF],C) = [TR % 1,RT,conj-antc(RF,toDNF(M,C))] && [TR % 2,RT,conj-antc(RF,toDNF(M,~ C))] .

  --- INP: ReachGoal
  --- PRE: None
  --- OUT: Set of goal's primary variables
  eq goal-pvars([TR,RT,RF]) = vars(term(antc(RF))) .

  --- INP: Module F:MatchAtom F':MatchForm
  --- PRE: MatchAtom and MatchForm are well-defined with respect to Module
  --- OUT: Sound approximation of formula F /\ ~ F'
  eq over-diff (M,T | C =>A D) = over-diff(M,none,T | C,D) .
endfm

mod REACH-PROOF is
  pr REACH-PROOF-STATE .
  pr RENAME-METAVARS .
  pr UNIT . --- constant noModule

  --- Step+Subsume Operators
  op stepGoals   : Module Module Nat RuleTrace Nat ReachFormSet RuleSubstSet ReachForm -> ReachGoalSet [format(d nn)] .
  op stepGoals*  : Module Module Nat RuleTrace Nat ReachFormSet RuleSubstSet ReachForm -> ReachGoalSet [format(d nn)] .
  op $stepGoal   : Module Module AbsRewRule Substitution ReachForm ReachGoal -> [ReachGoalSet] .
  op unify       : Module AbsRewRuleSet Term EqQFForm? ~> RuleSubstSet .
  op $unify1     : Module AbsRewRule SubstitutionSet Term EqQFForm? ~> RuleSubstSet .
  op filterUnsat : Module ReachFormSet -> ReachFormSet .
  op mustStep?   : Module Module MatchAtom MatchForm -> Bool .
  op mustStep*?  : [Bool] -> Bool .
  --- Axiom Operators
  op axiomGoals  : Module Nat RuleTrace Nat ReachFormSet ReachForm Substitution ReachForm ReachForm -> ReachGoalSet .
  op $axiomGoal  : MatchAtom ReachForm MatchForm Substitution ReachGoal -> ReachGoal .
  op implies     : Module Module MatchAtom MatchAtom -> SubstitutionSet .
  op $implies    : Module Module SubstitutionSet QFForm? QFForm? -> SubstitutionSet .
  op implies-msg : Module Module QFForm? -> SubstitutionSet .
  --- Miscellaneous Operators
  op quantErr    : MatchForm ReachFormSet -> ReachGoalSet .
  op quantErr1   : MatchForm ReachFormSet -> ReachFormSet .
  op quantErr2   : MatchForm ReachForm -> Bool .
  op getRFs      : ReachGoalSet -> ReachFormSet .
  op transform   : Module Module QFForm? -> QFForm? .
  op checkTrans  : QFForm? ~> QFForm? .
  op checkSat    : Module Module QFForm -> Bool .
  op makeImp     : QFForm? QFForm? -> QFForm? .
  op printImp    : Bool QFForm? QFForm? -> SubstitutionSet .

  var L R T T' T'' T1 T2 T3 T4 T5 : Term .
  var AXS AXS' RFS RFS' : ReachFormSet .
  var GLS GLS' GLS'' PGLS : ReachGoalSet .
  var GN N N1 N2 N3 N4 N5 : Nat .
  var RF RF' RF'' : ReachForm .
  var RLS ARS ARS' : AbsRewRuleSet .
  var D D' TRM : MatchForm .
  var QFF QFF' : QFForm .
  var C C' C'' P F : QFForm? .
  var ERR : [ReachGoalSet] .
  var RSS : RuleSubstSet .
  var SS : SubstitutionSet .
  var S S' : Substitution .
  var M M' M'' FM DM : Module .
  var AR : AbsRewRule .
  var MA : MatchAtom .
  var GL : ReachGoal .
  var RT : RuleType .
  var NL : RuleTrace .
  var TL : TermList .
  var FS : QFForm?Set .
  var STEP? B1 B2 : Bool .
  var QS : QidSet .
  var V : Variable .
  var KB : [Bool] .
  var Q : Qid .

  --- INP: M:MatchForm R:ReachForm
  --- PRE: M is head of original goal, R is a new goal generated after applying a rule
  --- OUT: true iff variable side condition for rules is preserved
 ceq quantErr(T | C,RFS)            = if RFS' == mtrfs then mt else rltl-error('Internal 'Error: 'Quantifier 'Check 'Failed,RFS') fi
  if RFS' := quantErr1(T | C,RFS) .
  eq quantErr1(T | C,RF & RFS)      = if quantErr2(T | C,RF) then RF else mtrfs fi & quantErr1(T | C,RFS) .
  eq quantErr1(T | C,mtrfs)         = mtrfs .
  eq quantErr2(T | C,T' | C' =>A D) = not intersection(vars(T | C),vars(D)) subset vars(T' | C') .

  --- INP: ReachGoalSet
  --- PRE: None
  --- OUT: ReachFormSet where each ReachForm comes from one ReachGoal
  eq getRFs([NL,RT,RF] && GLS) = RF & getRFs(GLS) .
  eq getRFs(mt)                = mtrfs .

  --- INP: Module:M Module:M' QFForm?
  --- PRE: M' should have a function reduce : Form -> Form which returns ff in case of any error
  ---      Form is wellFormed w.r.t to M
  --- OUT: Use M' to transform Form
  eq transform(M,noModule,QFF) = QFF  .
  eq transform(M,M',QFF)       = if QFF == ff then ff else checkTrans(downTerm(getTerm(metaReduce(M','reduce[upTerm(M),upTerm(QFF)])),ff)) fi .
 ceq checkTrans(QFF)           = QFF if QFF =/= ff .
  eq checkSat(M,M',QFF)        = var-sat(M,transform(M,M',toNNF(QFF))) .

  --- INP: Module Nat NL:RuleTrace GN:Nat AXS:ReachFormSet RSS:RuleSubstSet (T | C =>A D):RF:ReachForm
  --- PRE: RSS should be the result of unify
  --- OUT: A ReachGoalSet where each goal is of the form
  ---      [NL GN + I,axiom(AXS),(R | C /\ C' =>A D) << S,true]
  ---      where (rl L => R if C', S) is a rule/subst pair from RSS
  ---      and each C /\ C' is satisfiable in Module
  --- NB:  For each such goal, prints out its information to the terminal if
  ---      the print attribute is enabled
  eq stepGoals (M,M',N,NL,GN,AXS,RSS,T | C =>A D)          = stepGoals*(M,M',N,NL,GN,AXS,RSS,T | C =>A D) .
  eq stepGoals*(M,M',N,NL,GN,AXS,(AR,S) | RSS,T | C =>A D) =
    $stepGoal(M,M',AR,S,T | C =>A D,[NL @ GN,axioms(AXS),$renameForm(M,N,applySub(rhs(AR) | (C /\ cond(AR)) =>A D,S))]) &&
     stepGoals*(M,M',N,NL,s(GN),AXS,RSS,T | C =>A D) .
  eq stepGoals*(M,M',N,NL,GN,AXS,none,T | C =>A D)         = mt .
  ---
 ---ceq $stepGoal(M,M',rl Q : L => R if C and P .,S,RF',[NL,axioms(AXS),RF]) = [NL,axioms(AXS),removeGround(RF)]
 ceq $stepGoal(M,M',rl Q : L => R if C and P .,S,RF',[NL,axioms(AXS),RF]) = [NL,axioms(AXS),RF]
  if checkSat(M,M',cond(antc(RF)))
  .
  ---   [print "STEP:   " NL "\nFROM " RF' "\nBY   " Q "\nTO   " RF] .
  eq $stepGoal(M,M',AR,S,RF',[NL,axioms(AXS),RF]) = mt
  .
  ---   [owise print "SUBSUME: " NL] .

  --- INP: Nat NL:RuleTrace GN:Nat AXS:ReachFormSet RF':ReachForm S:Substitution (T | C =>A D):RF:ReachForm (T' | C' =>A D'):RF':ReachForm
  --- PRE: S should be the result of implies with the head of RF and RF'
  --- OUT: A ReachGoalSet with each contained ReachForm like RF except (T | C) is replaced
  ----     by (T'' << S | C /\ C'' << S) where (T'' | C'') is a subterm of D'
  --- NB:  For each such goal, prints out its information to the terminal if
  ---      the print attribute is enabled
  eq axiomGoals(M,N,NL,GN,AXS,RF',empty,RF,RF') = mt .
  eq axiomGoals(M,N,NL,GN,AXS,RF',S,RF,T' | C' =>A (T'' | C'') U D') =
    $axiomGoal(antc(RF),RF',T'' | C'',S,[NL # GN,axioms(AXS),$renameForm(M,N,T'' << S | (cond(antc(RF)) /\ C'' << S) =>A succ(RF))]) &&
    axiomGoals(M,N,NL,s(GN),AXS,RF',S,RF,T' | C' =>A D') .
  eq axiomGoals(M,N,NL,GN,AXS,RF',S,RF,T' | C' =>A (T'' | C'')) =
    $axiomGoal(antc(RF),RF',T'' | C'',S,[NL # GN,axioms(AXS),$renameForm(M,N,T'' << S | (cond(antc(RF)) /\ C'' << S) =>A succ(RF))]) .
  ---
  eq $axiomGoal(MA,RF',T'' | C'',S,[NL,axioms(AXS),RF]) = [NL,axioms(AXS),RF]
  .
  ---   [print "AXIOM:  " NL "\nFROM " MA  "\nBY   " RF' "\nTO   " RF] .

  --- INP: Module AbsRewRuleSet Term Form
  --- PRE: Arguments are well-defined with respect to Module
  --- OUT: All unifiers between the term and any rule that also satisfies
  ---      the conditions on both the term and that rule
  --- NB:  Renaming here is unsafe/shared vars between form/unifier are lost
  eq unify(M,rl Q : L => R if C' and P . ARS,T,C) =
    $unify1(M,rl Q : L => R if C' and P .,unifiers(M,L =? T),T,C) |
    unify(M,ARS,T,C) .
  eq unify(M,none,T,C) = none .
  eq $unify1(M,rl Q : L => R if C' and P .,S | SS,T,C) =
    --- rename here so any logical vars from unifiers() are removed;
    --- this way, sat function won't complain
    --- if var-sat(M,$renameForm(applySub(C',S) /\ applySub(C,S))) then
      (rl Q : L => R if C' and P .,S)
    --- else
    ---- none fi
    | $unify1(M,rl Q : L => R if C' and P .,SS,T,C) .
  eq $unify1(M,AR,empty,T,C) = none .

  --- INP: Module MatchAtom MatchForm
  --- PRE: Formulas are well-defined with respect to Module
  --- OUT: true if these formulas have an empty intersection
  eq mustStep?(M,M',T | C',TRM) = mustStep*?(not checkSat(M,M',falseId($renameForm(M,cond(and(M,T | C',TRM)))))) .
  eq mustStep*?(B1)             = B1 .
  eq mustStep*?(KB)             = KB [owise] .

  --- INP: Module F:MatchForm F':MatchForm
  --- PRE: MatchForm's well-defined with respect to module
  --- OUT: A substitution S for which witnesses that F implies F'
  ---      or empty, if no such substitution exists
  --- NB:  This function will print a warning if the implication is
  ---      vacuous whenever print attributes are enabled
  eq implies(M,M',T | C,T' | C') = $implies(M,M',matches(M,T',T),C,C') .
  eq $implies(M,M',S' | SS,C,C') =
    if not checkSat(M,M',makeImp(trueId(C),trueId(C' << S'))) then
      S' | implies-msg(M,M',C) | printImp(true,C,C' << S')
    else
      $implies(M,M',SS,C,C') | printImp(false,C,C' << S')
    fi .
  eq $implies(M,M',empty,C,C') = empty .
 ceq implies-msg(M,M',C) = empty if not checkSat(M,M',trueId(C)) [print "@@@@@@@@@@@@@@@@ Implication Vacuously Proven"] .
  eq implies-msg(M,M',C) = empty [owise] .

  eq makeImp(QFF,QFF')    = ~(QFF => QFF')  .
  eq printImp(true,C,C')  = empty [print "  VALID IMP: " C " => " C'] .
  eq printImp(false,C,C') = empty [print "INVALID IMP: " C " => " C'] .

 crl [step&subsume] :
     [M,FM,DM,N,TRM,RLS,AXS || [NL,step,T | C =>A D] && GLS || PGLS]
  =>
     [M,FM,DM,N,TRM,RLS,AXS || GLS || PGLS && GLS' && ERR]
  --- collect substitutions we need
  if RSS   := unify(M,RLS,T,C)
  /\ C'    := over-diff(M,none,T | C,D)
  --- generate new goals
  /\ GLS'  := stepGoals(M,DM,N,NL,1,AXS,RSS,T | C' =>A D)
  --- generate new errors
           --- if we have any goals, we must show every instance of our configuration can take a step
           --- this is required because we perform unification with each rule --- thus we cannot in general
           --- know if our unifiers cover every instance of our original term
  /\ ERR   := if GLS' =/= mt implies mustStep?(M,DM,T | C',TRM) then mt else rltl-error('Can 'Terminate 'Before 'Reaching 'Goal,T | C') fi
           --- if no rules apply at all, it must be that we successfully subsumed
           --- this follows because when we reach here, it means no axiom or semantic rule can apply
           --- if we cannot subsume to complete the proof, it means we are stuck
           && if RSS == none implies not checkSat(M,DM,falseId(C')) then mt else rltl-error('Stuck; 'Cannot 'Apply 'Any 'Rule`, 'Axiom`, 'Or 'Subsume,NL,T | C =>A D) fi
           --- if a variable moves from one side to the other, that means it changed from universal
           --- to existential, which doesn't make any sense --- thus this should never happen
           . --- && quantErr(T | C',getRFs(GLS'))

 crl [axiom] :
     [M,FM,DM,N,TRM,RLS,AXS || [NL,axioms(RF' & AXS'),RF] && GLS || PGLS]
  =>
     if B1 and GLS' == mt then
       [M,FM,DM,N,TRM,RLS,AXS || [NL,axioms(AXS'),RF] && GLS || PGLS]
     else
       [M,FM,DM,N,TRM,RLS,AXS || GLS || PGLS && GLS' && ERR]
     fi
  --- check if we are done
  if B1    := checkSat(M,DM,falseId(over-diff(M,RF)))
  --- collect substitutions we need
  /\ SS    := if B1 then implies(M,DM,antc(RF),antc(RF')) else empty fi
  --- generate new goals & filter generated goals for debugging
  /\ GLS'  := axiomGoals(M,N,NL,1,AXS,RF',SS,RF,RF')
  --- generate new errors
  /\ ERR   := mt . ---quantErr(antc(RF),getRFs(GLS')) .

  rl [axiom-done] :
     [M,FM,DM,N,TRM,RLS,AXS || [NL,axioms(mtrfs),T | C =>A D] && GLS || PGLS]
  =>
     [M,FM,DM,N,TRM,RLS,AXS || [NL,step,T | C =>A D] && GLS || PGLS] .

  --- Extra equation for helping automate debugging
  eq [M,FM,DM,N,TRM,RLS,AXS || [NL,axioms(...),RF] && GLS || PGLS] = [M,FM,DM,N,TRM,RLS,AXS || [NL,axioms(AXS),RF] && GLS || PGLS] .
endm
