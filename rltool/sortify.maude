--- name: sortify.maude
--- reqs: prelude, full-maude
--- desc: This module implements functionality to eliminate all mentions of
---       kinds, e.g. [KindName], from a module, and replace them with a sort
---       that is the supersort of all other sorts in the kind; since introducting
---       new sort names is not a safe operation because of the possibility of
---       introducing a name conflict, the module is parameterized over
---       a theory which provides a renaming function. Thus, the user can
---       design a renaming function which fits their application.
---
---       The module also contains code to check that a module does not mention
---       kinds at all. In that case, sortify conversion is not necessary.
---       The module KIND-CHECK contains the function kinds?() which when given
---       a piece of a module that could contain kinds, it will check if it
---       does. It has a boolean flag which controls how strictly it checks:
---       if the flag is false, it will only check for direct occurences of
---       kinds in the program text. If true, it will check that all terms
---       appearing in the program have a least type that is not a kind.
---
---       Note that, to properly sortify a theory, we need to rename:
---         [1] Kinds
---         [2] Terms
---         [3] Attrs
---         [4] Conditions
---         [5] Operators
---         [6] Equations
---         [7] Memberships
---         [8] Rules
---
---       Example Instantiation of SORTIFY-IMPL:
---
---       fmod LOWER-KIND-EX-IMPL is
---         pr META-LEVEL .
---         pr CONVERSION .
---         pr STR-REPLACE .
---         op lower-kind : Module Kind -> Sort .
---         var M : Module .
---         var K : Kind .
---         eq lower-kind(M,K) = 'NewKindName`(M,K`) .
---       endfm
---
---       view LowerKindEx from LOWER-KIND to LOWER-KIND-EX-IMPL is endv
---
---       fmod SORTIFY-EX is
---         pr SORTIFY-IMPL{LowerKindEx} .
---       endfm

fth LOWER-KIND is
  pr META-MODULE .
  op lower-kind : Module Kind -> Sort .
endfth

fmod SORTIFY-IMPL{X :: LOWER-KIND} is
  pr META-MODULE .
  pr QID-JOIN .
  pr UNIT .

  --- Lowers kinds types and terms
  op lower-type    : Module TypeListSet -> TypeListSet .
  op lower-term    : Module TermList -> TermList .

  --- Injects lowered Kinds into Subsort Order
  op inject-kinds  : Module -> SubsortDeclSet .
  op inject-kinds  : Module SortSet -> SubsortDeclSet .

  --- Traverses module structure
  op sortify : Module AttrSet -> AttrSet .
  op sortify : Module Condition -> Condition .
  op sortify : Module OpDeclSet -> OpDeclSet .
  op sortify : Module MembAxSet -> MembAxSet .
  op sortify : Module EquationSet -> EquationSet .
  op sortify : Module RuleSet -> RuleSet .
  op sortify : Module -> Module .

  var FM     : FModule .
  var FT     : FTheory .
  var SM     : SModule .
  var ST     : STheory .
  var M      : Module .
  var K      : Kind .
  var S      : Sort .
  var SS     : SortSet .
  var T      : Type .
  var TL TL' : TypeList .
  var NTL    : NeTypeList .
  var TS     : TypeListSet .
  var C      : Constant .
  var V      : Variable .
  var TML    : TermList .
  var NTML   : NeTermList .
  var Q      : Qid .
  var AS     : AttrSet .
  var CN     : Condition .
  var TM TM' : Term .
  var O O'   : OpDecl .
  var OS     : OpDeclSet .
  var E E'   : Equation .
  var ES     : EquationSet .
  var MB MB' : MembAx .
  var MBS    : MembAxSet .
  var R R'   : Rule .
  var RS     : RuleSet .

  eq lower-type(M,S)              = S .
  eq lower-type(M,K)              = lower-kind(M,completeName(M,K)) .
  eq lower-type(M,T NTL)          = lower-type(M,T) lower-type(M,NTL) .
  eq lower-type(M,nil)            = nil .
  eq lower-type(M,TL ; TL' ; TS)  = lower-type(M,TL) ; lower-type(M,TL' ; TS) .
  eq lower-type(M,none)           = none .

  eq lower-term(M,V)         = join(getName(V) ': lower-type(M,getType(V))) .
  eq lower-term(M,C)         = join(getName(C) '. lower-type(M,getType(C))) .
  eq lower-term(M,Q[NTML])   = Q[lower-term(M,NTML)] .
  eq lower-term(M,(TM,NTML)) = lower-term(M,TM), lower-term(M,NTML) .
  eq lower-term(M,empty)     = empty .

  eq inject-kinds(M)        = inject-kinds(M,getSorts(M)) .
  eq inject-kinds(M,S ; SS) = subsort S < lower-kind(M,getKind(M,S)) . inject-kinds(M,SS) .
  eq inject-kinds(M,none)   = none .

  eq sortify(M,id(T) AS)       = id(lower-term(M,T)) sortify(M,AS) .
  eq sortify(M,left-id(T) AS)  = left-id(lower-term(M,T)) sortify(M,AS) .
  eq sortify(M,right-id(T) AS) = right-id(lower-term(M,T)) sortify(M,AS) .
  eq sortify(M,AS)             = AS [owise] .

  eq sortify(M,TM = TM' /\ CN)  = lower-term(M,TM) = lower-term(M,TM') /\ sortify(M,CN) .
  eq sortify(M,TM : S /\ CN)    = lower-term(M,TM) : S /\ sortify(M,CN) .
  eq sortify(M,TM := TM' /\ CN) = lower-term(M,TM) := lower-term(M,TM') /\ sortify(M,CN) .
  eq sortify(M,TM => TM' /\ CN) = lower-term(M,TM) => lower-term(M,TM') /\ sortify(M,CN) .
  eq sortify(M,nil)             = nil .

  eq sortify(M,op Q : TL -> T [AS].) = op Q : lower-type(M,TL) -> lower-type(M,T) [sortify(M,AS)]. .
  eq sortify(M,O O' OS)              = sortify(M,O) sortify(M,O' OS) .
  eq sortify(M,(none).OpDeclSet)     = (none).OpDeclSet .

  eq sortify(M,mb T : S [AS].)        = mb lower-term(M,T) : S [sortify(M,AS)]. .
  eq sortify(M,cmb T : S if CN [AS].) = cmb lower-term(M,T) : S if sortify(M,CN) [sortify(M,AS)]. .
  eq sortify(M,MB MB' MBS)            = sortify(M,MB) sortify(M,MB' MBS) .
  eq sortify(M,(none).MembAxSet)      = (none).MembAxSet .

  eq sortify(M,eq TM = TM' [AS].)        = eq lower-term(M,TM) = lower-term(M,TM') [sortify(M,AS)]. .
  eq sortify(M,ceq TM = TM' if CN [AS].) = ceq lower-term(M,TM) = lower-term(M,TM') if sortify(M,CN) [sortify(M,AS)]. .
  eq sortify(M,E E' ES)                  = sortify(M,E) sortify(M,E' ES) .
  eq sortify(M,(none).EquationSet)       = (none).EquationSet .

  eq sortify(M,rl TM => TM' [AS].)        = rl lower-term(M,TM) => lower-term(M,TM') [sortify(M,AS)]. .
  eq sortify(M,crl TM => TM' if CN [AS].) = crl lower-term(M,TM) => lower-term(M,TM') if sortify(M,CN) [sortify(M,AS)]. .
  eq sortify(M,R R' RS)                   = sortify(M,R) sortify(M,R' RS) .
  eq sortify(M,(none).RuleSet)            = (none).RuleSet .

  eq sortify(FM) =
    addSorts(lower-type(FM,getKinds(FM)),
      addSubsorts(inject-kinds(FM),
        setOps(
          setMbs(
            setEqs(FM,sortify(FM,getEqs(FM))),
          sortify(FM,getMbs(FM))),
        sortify(FM,getOps(FM))))) .

  eq sortify(FT) =
    addSorts(lower-type(FT,getKinds(FT)),
      addSubsorts(inject-kinds(FT),
        setOps(
          setMbs(
            setEqs(FT,sortify(FT,getEqs(FT))),
          sortify(FT,getMbs(FT))),
        sortify(FT,getOps(FT))))) .

  eq sortify(SM) =
    addSorts(lower-type(SM,getKinds(SM)),
      addSubsorts(inject-kinds(SM),
        setOps(
          setMbs(
            setEqs(SM,sortify(SM,getEqs(SM))),
          sortify(SM,getMbs(SM))),
        sortify(SM,getOps(SM))))) .

  eq sortify(ST) =
    addSorts(lower-type(ST,getKinds(ST)),
      addSubsorts(inject-kinds(ST),
        setOps(
          setMbs(
            setEqs(ST,sortify(ST,getEqs(ST))),
          sortify(ST,getMbs(ST))),
        sortify(ST,getOps(ST))))) .
endfm

fmod KIND-CHECK is
  pr META-LEVEL .

  --- copy of or-else to make eqs more readable
  op _orL_ : Bool Bool -> Bool [strat (1 0) gather (e E) prec 59] .

  op kinds? : TypeListSet -> Bool .
  op kinds? : Module Bool TermList -> Bool .
  op kinds? : Module Bool AttrSet -> Bool .
  op kinds? : Module Bool Condition -> Bool .
  op kinds? : Module Bool OpDeclSet -> Bool .
  op kinds? : Module Bool MembAxSet -> Bool .
  op kinds? : Module Bool EquationSet -> Bool .
  op kinds? : Module Bool RuleSet -> Bool .
  op kinds? : Bool Module -> Bool .

  var FM     : FModule .
  var FT     : FTheory .
  var SM     : SModule .
  var ST     : STheory .
  var M      : Module .
  var K      : Kind .
  var S      : Sort .
  var SS     : SortSet .
  var T      : Type .
  var TL TL' : TypeList .
  var NTL    : NeTypeList .
  var TS     : TypeListSet .
  var C      : Constant .
  var V      : Variable .
  var TML    : TermList .
  var NTML   : NeTermList .
  var Q      : Qid .
  var AS     : AttrSet .
  var CN     : Condition .
  var TM TM' : Term .
  var O O'   : OpDecl .
  var OS     : OpDeclSet .
  var E E'   : Equation .
  var ES     : EquationSet .
  var MB MB' : MembAx .
  var MBS    : MembAxSet .
  var R R'   : Rule .
  var RS     : RuleSet .
  var B      : Bool .

  eq true orL B = true .
  eq false orL B = B .

  eq kinds?(K)                  = true .
  eq kinds?(S)                  = false .
  eq kinds?(T NTL)              = kinds?(T) orL kinds?(NTL) .
  eq kinds?(nil)                = false .
  eq kinds?(TL ; TL' ; TS)      = kinds?(TL) orL kinds?(TL' ; TS) .
  eq kinds?((none).TypeListSet) = false .

  eq kinds?(M,B,C)         = getType(C) :: Kind .
  eq kinds?(M,B,V)         = getType(V) :: Kind .
  eq kinds?(M,B,Q[NTML])   = (B and-then (leastSort(M,Q[NTML]) :: Kind)) orL kinds?(M,B,NTML) .
  eq kinds?(M,B,(TM,NTML)) = kinds?(M,B,TM) orL kinds?(M,B,NTML) .
  eq kinds?(M,B,empty)     = false .

  eq kinds?(M,B,id(T) AS)       = kinds?(M,B,T) orL kinds?(M,B,AS) .
  eq kinds?(M,B,left-id(T) AS)  = kinds?(M,B,T) orL kinds?(M,B,AS) .
  eq kinds?(M,B,right-id(T) AS) = kinds?(M,B,T) orL kinds?(M,B,AS) .
  eq kinds?(M,B,AS)             = false [owise] .

  eq kinds?(M,B,TM = TM' /\ CN)  = kinds?(M,B,(TM,TM')) orL kinds?(M,B,CN) .
  eq kinds?(M,B,TM : S /\ CN)    = kinds?(M,B,TM) orL kinds?(M,B,CN) .
  eq kinds?(M,B,TM := TM' /\ CN) = kinds?(M,B,(TM,TM')) orL kinds?(M,B,CN) .
  eq kinds?(M,B,TM => TM' /\ CN) = kinds?(M,B,(TM,TM')) orL kinds?(M,B,CN) .
  eq kinds?(M,B,(nil).Condition) = false .

  eq kinds?(M,B,op Q : TL -> T [AS].) = kinds?(TL T) orL kinds?(M,B,AS) .
  eq kinds?(M,B,O O' OS)              = kinds?(M,B,O) orL kinds?(M,B,O' OS) .
  eq kinds?(M,B,(none).OpDeclSet)     = false .

  eq kinds?(M,B,mb TM : S [AS].)        = kinds?(M,B,TM) orL kinds?(M,B,AS) .
  eq kinds?(M,B,cmb TM : S if CN [AS].) = kinds?(M,B,TM) orL kinds?(M,B,CN) orL kinds?(M,B,AS) .
  eq kinds?(M,B,MB MB' MBS)             = kinds?(M,B,MB) orL kinds?(M,B,MB' MBS) .
  eq kinds?(M,B,(none).MembAxSet)       = false .

  eq kinds?(M,B,eq TM = TM' [AS].)        = kinds?(M,B,(TM,TM')) orL kinds?(M,B,AS) .
  eq kinds?(M,B,ceq TM = TM' if CN [AS].) = kinds?(M,B,(TM,TM')) orL kinds?(M,B,CN) orL kinds?(M,B,AS) .
  eq kinds?(M,B,E E' ES)                  = kinds?(M,B,E) orL kinds?(M,B,E' ES) .
  eq kinds?(M,B,(none).EquationSet)       = false .

  eq kinds?(M,B,rl TM => TM' [AS].)        = kinds?(M,B,(TM,TM')) orL kinds?(M,B,AS) .
  eq kinds?(M,B,crl TM => TM' if CN [AS].) = kinds?(M,B,(TM,TM')) orL kinds?(M,B,CN) orL kinds?(M,B,AS) .
  eq kinds?(M,B,R R' RS)                   = kinds?(M,B,R) orL kinds?(M,B,R' RS) .
  eq kinds?(M,B,(none).RuleSet)            = false .

  eq kinds?(B,FM) =
    kinds?(FM,B,getOps(FM)) orL
    kinds?(FM,B,getMbs(FM)) orL
    kinds?(FM,B,getEqs(FM)) .

  eq kinds?(B,FT) =
    kinds?(FT,B,getOps(FT)) orL
    kinds?(FT,B,getMbs(FT)) orL
    kinds?(FT,B,getEqs(FT)) .

  eq kinds?(B,SM) =
    kinds?(SM,B,getOps(SM)) orL
    kinds?(SM,B,getMbs(SM)) orL
    kinds?(SM,B,getEqs(SM)) orL
    kinds?(SM,B,getRls(SM)) .

  eq kinds?(B,ST) =
    kinds?(ST,B,getOps(ST)) orL
    kinds?(ST,B,getMbs(ST)) orL
    kinds?(ST,B,getEqs(ST)) orL
    kinds?(ST,B,getRls(ST)) .
endfm
