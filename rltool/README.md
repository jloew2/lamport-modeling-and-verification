# Reachability Logic Tool Readme

## Introduction

This tool is designed to help a user verify reachability logic properites over
rewrite theories, developed according to the theoretical framework provided by
the paper: _A Constructor-Based Reachability Logic for Rewrite Theories_.

By using rewriting logic as our semantic foundation, we have
a diverse array of tools at our disposal to help us analyze our specifications.
Additionally, we can make use of notions such as constructors which are not
available in the fully general reachability logic framework.

**Note 1**: this is the first release of this tool. Though we have made every
effort to verify its correctness on different examples, there will still be
bugs.

This tool provides a FULL-MAUDE and LOOP-MODE based user-interface.
The user should provide input according to the command grammar available
in the appendix in our [technical report][techrep].

Essentially, the user must input the name of a module to reason about,
a pattern defining the set of terminating states, and a set of goals
to prove. 

## Usage

In this section, we assume the user knows about basic Maude concepts
such as a module, equation, and rewrite rule. If you are unfamiliar
with these concepts, you can find a complete explanation in the
[Maude manual][Maude Manual].

In the description below, let $ROOTDIR denote the root directory where
the installation was unpacked. To run the tool, open your terminal of your
choice and type:

`cd $ROOTDIR`
`./maude rltool.maude`

This will cause the Maude interpreter to load the file `rltool.maude` which
in turns loads all the files/modules required by the tool. At this point,
you should see the rltool welcome banner as well as the Maude prompt `Maude>`
and should be able to enter Maude commands. To interact with the tool,
input commands according to the grammar in the [technical report][techrep].

Here is the proof script for the CHOICE example, located at
`$ROOTDIR/ex/choice/choice.maude`:

`(select CHOICE .)
(declare-vars (M:MSet) U (N:Nat) .)
(def-term-set ({N}) | true .)
(add-goal ({M}) | true =>A ({N}) | (N =C M) = (tt) .)
(start-proof .)
(case 1 on M@3:MSet by (M1:MSet M2:MSet) U (N':Nat) .)
(step* .)`

## Examples

This distribution comes with a few examples you can run to understand
how to provide input to the tool and analyze how it makes progress.
The examples are provided in the `$ROOTDIR/ex`, where each subdirectory
corresponds to one or more example(s). To run an example, type:

`cd $ROOTDIR`
`./maude ex/$TEST/$EXAMPLE_FILE.maude`

To see intermediate proof steps instead of just the final proof state,
use the `(step .)` command instead of the `(step* .)` command.

## Common Execution Problems

The Maude binary is dynamically linked and depends on a variety of linux
libraries; most of them are fairly basic, just the C/C++ runtime and the
linux loader. Some distributions (particularly older RedHat derivatives)
may not have recent enough C/C++ runtime libraries. In this case, the
best course of action is to install newer runtime libraries (typically
requires administrator access) or to use a different linux distribution.

However, this binary also depends on the a shared library libtinfo.so.5.
This library is actually just a renaming of libncurses.so.5. A precompiled
version is included with this distribution for your convenience with
a wrapper script to invoke it. If you have problems, you can directly
use the libncurses.so.5 available on your system.


[Maude Manual]: http://maude.cs.illinois.edu/w/index.php?title=Maude_Manual_and_Examples
[techrep]: https://www.ideals.illinois.edu/handle/2142/95770
